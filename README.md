formatters
==========

These formatters are meant to be used by the [`git-checks`][] and
[`rust-ghostflow`][] crates for performing formatting guidelines to a codebase.
They are written to be safe and adhere to the guidelines put forth in the
documentation of those crates.

Generally, the formatters should be fail-safe and handle all error cases
gracefully because when they are used to rewrite a branch, they may
inadvertently introduce bugs. in addition, they should be idempotent and not
generate diffs when run multiple times.

[`git-checks`]: https://gitlab.kitware.com/utils/rust-git-checks
[`rust-ghostflow`]: https://gitlab.kitware.com/utils/rust-ghostflow
